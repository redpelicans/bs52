const base52 = require('..');
const baseX = require('base-x');
const fixtures = [
  'coucou',
  '',
  'ABS 123(Top)ics ﻮﺣﺩﺓ',
  'éé&#oô/+_ ,ç'
]

const BASE52 = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
const bx52 = baseX(BASE52);

const runTest = input => {
  it(`should encode "${input}"`, () => {
    expect(base52.encode(input)).toEqual(bx52.encode(Buffer.from(input)).toString());
  });

  const res = bx52.encode(Buffer.from(input)).toString();

  it(`should decode "${res}"`, () => {
    expect(base52.decode(res)).toEqual(input);
  });
};

describe('base52', () => {
  it('should encode and decode', () => {
    const input = 'coucou';
    expect(base52.decode(base52.encode(input))).toEqual(input);
  });

  describe('should get same results as base-x', () => {
    for(let input of fixtures) runTest(input);
  });
});
