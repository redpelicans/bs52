module.exports = {
  verbose: true,
  testEnvironment: 'node',
  collectCoverageFrom: ['src/*.js'],
  coverageThreshold: {
    global: {
      statements: 98,
      branches: 91,
      functions: 98,
      lines: 98,
    },
  },
  moduleDirectories: ['node_modules'],
  testRegex: 'tests/.*\\.test\\.js$',
};
