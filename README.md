# bs52

[![NPM Package](https://img.shields.io/npm/v/bs52.svg?style=flat-square)](https://www.npmjs.org/package/bs52)
[![Build Status](https://gitlab.com/redpelicans/bs52/badges/master/pipeline.svg)](https://gitlab.com/redpelicans/bs52/pipelines)


Fast base52 strings encoding / decoding using bitcoin style leading zero compression.


`bs52` is a rewrite of [cryptocoinjs/base-x](https://github.com/cryptocoinjs/base-x) without `Buffer` and limited to base52 and strings as input.

Target was to use the library in browsers.

@redpelicans we are using this lib to encode/decode keys using extended chars.

It's limited to below alphabet:

```
  const ALPHABET = `abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`;
```

## Example


``` javascript
var bs52 = require('bs52');

var decoded = bs52.encode('Bonjour ici les bidibules')
// sTSzrBprGARdjdZHPiCpfcASBQxaCTEnHmZ

console.log(bs52.decode('sTSzrBprGARdjdZHPiCpfcASBQxaCTEnHmZ'))
// Bonjour ici ...
```


